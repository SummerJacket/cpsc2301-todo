/**
 * Sample React Native App TODO listview
 * from: https://github.com/hellokoding/todoapp-reactnative
 * from: https://hellokoding.com/todo-app-with-react-native/
 */

// npm install react-native-sortable-listview --save

import React from 'react';
import { AppRegistry, StyleSheet, Text, View } from 'react-native';
import ListView from './src/ListView.js';

export default class App extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<ListView />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		paddingTop: 30,
		paddingBottom: 10,
		paddingLeft: 2,
		paddingRight: 2,
		backgroundColor: '#F8F8F8',
	},
});
